### Movies 


#### How to run the project 



```bash 

$- git clone https://gitlab.com/omrani.rihab/movies.git

$- cd movies/

$- docker-compose up -d --build

$- docker-compose exec php /bin/bash

$- symfony console doctrine:database:create  

$- symfony console doctrine:migrations:migrate                

$- symfony serve -d 
```


##### Additional infos 

###### services : 

MailCatcher url : [http://127.0.0.1:1080/](http://127.0.0.1:1080/)

database: :

```bash

$- docker-compose exec database /bin/bash 

$- mysql -u root -p symfony_docker #password = secret 


```
