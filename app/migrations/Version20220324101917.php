<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220324101917 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE film (id INT AUTO_INCREMENT NOT NULL, adult TINYINT(1) NOT NULL, backdrop_path VARCHAR(255) NOT NULL, original_language VARCHAR(2) NOT NULL, original_title VARCHAR(255) NOT NULL, overview LONGTEXT NOT NULL, popularity DOUBLE PRECISION NOT NULL, poster_path VARCHAR(255) NOT NULL, release_date VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, video TINYINT(1) NOT NULL, vote_average DOUBLE PRECISION NOT NULL, vote_count INT NOT NULL, film_id INT NOT NULL, count INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE film_genre_ids (film_id INT NOT NULL, genre_ids_id INT NOT NULL, INDEX IDX_A2880A1E567F5183 (film_id), INDEX IDX_A2880A1EF9B20DCC (genre_ids_id), PRIMARY KEY(film_id, genre_ids_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE genre_ids (id INT AUTO_INCREMENT NOT NULL, genre_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE film_genre_ids ADD CONSTRAINT FK_A2880A1E567F5183 FOREIGN KEY (film_id) REFERENCES film (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE film_genre_ids ADD CONSTRAINT FK_A2880A1EF9B20DCC FOREIGN KEY (genre_ids_id) REFERENCES genre_ids (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE film_genre_ids DROP FOREIGN KEY FK_A2880A1E567F5183');
        $this->addSql('ALTER TABLE film_genre_ids DROP FOREIGN KEY FK_A2880A1EF9B20DCC');
        $this->addSql('DROP TABLE film');
        $this->addSql('DROP TABLE film_genre_ids');
        $this->addSql('DROP TABLE genre_ids');
    }
}
