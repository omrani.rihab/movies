<?php

namespace App\Repository;

use App\Entity\GenreIds;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GenreIds|null find($id, $lockMode = null, $lockVersion = null)
 * @method GenreIds|null findOneBy(array $criteria, array $orderBy = null)
 * @method GenreIds[]    findAll()
 * @method GenreIds[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GenreIdsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GenreIds::class);
    }

    // /**
    //  * @return GenreIds[] Returns an array of GenreIds objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GenreIds
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
