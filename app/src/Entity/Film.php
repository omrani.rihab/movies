<?php

namespace App\Entity;

use App\Repository\FilmRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FilmRepository::class)]
class Film
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean')]
    private $adult;

    #[ORM\Column(type: 'string', length: 255)]
    private $backdropPath;

    #[ORM\Column(type: 'string', length: 2)]
    private $originalLanguage;

    #[ORM\Column(type: 'string', length: 255)]
    private $originalTitle;

    #[ORM\Column(type: 'text')]
    private $overview;

    #[ORM\Column(type: 'float')]
    private $popularity;

    #[ORM\Column(type: 'string', length: 255)]
    private $posterPath;

    #[ORM\Column(type: 'string', length: 255)]
    private $releaseDate;

    #[ORM\Column(type: 'string', length: 255)]
    private $title;

    #[ORM\Column(type: 'boolean')]
    private $video;

    #[ORM\Column(type: 'float')]
    private $voteAverage;

    #[ORM\Column(type: 'integer')]
    private $voteCount;

    #[ORM\Column(type: 'integer')]
    private $filmId;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $count;

    #[ORM\ManyToMany(targetEntity: GenreIds::class)]
    private $genreIds;

    public function __construct()
    {
        $this->genreIds = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAdult(): ?bool
    {
        return $this->adult;
    }

    public function setAdult(bool $adult): self
    {
        $this->adult = $adult;

        return $this;
    }

    public function getBackdropPath(): ?string
    {
        return $this->backdropPath;
    }

    public function setBackdropPath(string $backdropPath): self
    {
        $this->backdropPath = $backdropPath;

        return $this;
    }

    public function getOriginalLanguage(): ?string
    {
        return $this->originalLanguage;
    }

    public function setOriginalLanguage(string $originalLanguage): self
    {
        $this->originalLanguage = $originalLanguage;

        return $this;
    }

    public function getOriginalTitle(): ?string
    {
        return $this->originalTitle;
    }

    public function setOriginalTitle(string $originalTitle): self
    {
        $this->originalTitle = $originalTitle;

        return $this;
    }

    public function getOverview(): ?string
    {
        return $this->overview;
    }

    public function setOverview(string $overview): self
    {
        $this->overview = $overview;

        return $this;
    }

    public function getPopularity(): ?float
    {
        return $this->popularity;
    }

    public function setPopularity(float $popularity): self
    {
        $this->popularity = $popularity;

        return $this;
    }

    public function getPosterPath(): ?string
    {
        return $this->posterPath;
    }

    public function setPosterPath(string $posterPath): self
    {
        $this->posterPath = $posterPath;

        return $this;
    }

    public function getReleaseDate(): ?string
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(string $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getVideo(): ?bool
    {
        return $this->video;
    }

    public function setVideo(bool $video): self
    {
        $this->video = $video;

        return $this;
    }

    public function getVoteAverage(): ?float
    {
        return $this->voteAverage;
    }

    public function setVoteAverage(float $voteAverage): self
    {
        $this->voteAverage = $voteAverage;

        return $this;
    }

    public function getVoteCount(): ?int
    {
        return $this->voteCount;
    }

    public function setVoteCount(int $voteCount): self
    {
        $this->voteCount = $voteCount;

        return $this;
    }

    public function getFilmId(): ?int
    {
        return $this->filmId;
    }

    public function setFilmId(int $filmId): self
    {
        $this->filmId = $filmId;

        return $this;
    }

    public function getCount(): ?int
    {
        return $this->count;
    }

    public function setCount(?int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return Collection|GenreIds[]
     */
    public function getGenreIds(): Collection
    {
        return $this->genreIds;
    }

    public function addGenreId(GenreIds $genreId): self
    {
        if (!$this->genreIds->contains($genreId)) {
            $this->genreIds[] = $genreId;
        }

        return $this;
    }

    public function removeGenreId(GenreIds $genreId): self
    {
        $this->genreIds->removeElement($genreId);

        return $this;
    }
}
