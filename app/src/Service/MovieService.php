<?php

declare(strict_types=1);

namespace App\Service;


use App\Entity\Film;
use App\Entity\Genre;
use App\Entity\GenreIds;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

/**
 * Class MovieService
 *
 * @package App\Service
 */
class MovieService
{
    /**
     * @var string
     */
    private const API_KEY = 'c89646cb9c2f9f7a6144c074fff0e9c7';

    /**
     * @var EntityManagerInterface
     */
    public EntityManagerInterface $entityManager;


    function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $url
     *
     * @return array|null
     */
    private function getMoviesInfos(string $url): ?array
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        $result = json_decode($result, true);

        if (true === isset($result['success']) && (bool)$result['success'] === false) {
            return null;
        }

        curl_close($curl);

        return $result;
    }

    /**
     * @param EntityManager $entityManager
     *
     * @return array
     * @throws OptimisticLockException
     *
     * @throws ORMException
     */
    final function saveMovies(EntityManager $entityManager): array
    {

        $url = 'https://api.themoviedb.org/3/movie/now_playing?api_key=';
        $url .= self::API_KEY;
        $movies = [];

        $films = $entityManager->getRepository(Film::class)->findAll();

        if (count($films) > 0) {
            $result = $films;
        } else {
            $result = $this->getMoviesInfos($url)['results'];

            foreach ($result as $row) {
                $item = new Film();

                $item->setAdult($row['adult'])
                    ->setBackdropPath($row['backdrop_path'])
                    ->setFilmId((int)$row['id'])
                    ->setOriginalLanguage($row['original_language'])
                    ->setOriginalTitle((string)$row['original_title'])
                    ->setOverview($row['overview'])
                    ->setPopularity($row['popularity'])
                    ->setPosterPath($row['poster_path'])
                    ->setReleaseDate($row['release_date'])
                    ->setTitle($row['title'])
                    ->setVideo($row['video'])
                    ->setVoteAverage($row['vote_average'])
                    ->setVoteCount($row['vote_count']);

//                dd($row['genre_ids']);
                foreach ($row['genre_ids'] as $id) {

                    $genreId = (new GenreIds())
                        ->setGenreId($id)
                        ->addFilm($item)
                        ;
                    $entityManager->persist($genreId);
                }

                $movies[] = $item;
                $entityManager->persist($item);
            }


        }

        $entityManager->flush();

        return $movies;
    }
}
