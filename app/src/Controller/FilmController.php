<?php

namespace App\Controller;

use App\Entity\Film;
use App\Repository\FilmRepository;
use App\Service\MovieService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Slim\Exception\NotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class FilmController extends AbstractController
{
    /**
     * @var string
     */
    private const API_KEY = 'c89646cb9c2f9f7a6144c074fff0e9c7';

    /**
     * @var EntityManagerInterface
     */
    public EntityManagerInterface $entityManager;

    /**
     * @var MovieService
     */
    private MovieService $movieService;

    /**
     * @var MailerInterface
     */
    private MailerInterface $mailer;

    function __construct(EntityManagerInterface $entityManager, MovieService $movieService, MailerInterface $mailer)
    {
        $this->entityManager = $entityManager;
        $this->movieService = $movieService;
        $this->mailer = $mailer;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    #[Route('/', name: 'app.index')]
    public function index(
        FilmRepository $filmRepository
    )
    : Response {
        $url = 'https://api.themoviedb.org/3/movie/now_playing?api_key='.self::API_KEY;
        $movies = $this->movieService->saveMovies($this->entityManager);
        $topThreeFilms = $this->entityManager->getRepository(Film::class)->findTopThreeMovies();

        return $this->render(
            'film/index.html.twig',
            [
                'films' => $this->entityManager->getRepository(Film::class)->findOtherFilms(),
                'topThreeFilms' => $topThreeFilms,
            ]
        );
    }

    #[Route('/film/{id}', name: 'app.film.details')]
    public function show(int $id): Response
    {
        $film = $this->entityManager->getRepository(Film::class)->findOneBy(array('filmId' => $id));

        if( null === $film ) {
            return $this->render('film/404.html.twig');
        }
        return $this->render('film/details.html.twig', [
            'film' => $film
        ]);
    }

    /**
     * @param integer $id
     * @param MailerInterface $mailer
     *
     * @return Response
     */
    #[Route('/film/{id}/adress', name: 'app.film.send')]
    public function send(int $id, MailerInterface $mailer): Response
    {
        $film= $this->entityManager->getRepository(Film::class)->findOneBy(['filmId' => $id]);
        $email = (new Email())
            ->from('omrani.rihab.eser@gmail.com')
            ->to('aminelch@pm.me')
            ->subject('Movies')
            ->text('http://localhost:8000/film/'.$id)
            ->html('<p>See Movie!</p>');

        try {
            $mailer->send($email);
            $film->setCount($film->getCount()+1);
            $this->entityManager->persist($film);
            $this->entityManager->flush();
        } catch (TransportExceptionInterface $e) {

        }

        return $this->redirectToRoute('app.film.details', ['id' => $id]);
    }
}
